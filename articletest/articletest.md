When we first read the word “écocritique”, we might be tempted to assume
that the book will be a call for new ecological and green practices
(especially in the case of such an unknown discipline). But S. Posthumus
leaves no room for us to reduce her work to a report of environmental
issues. From the beginning to the end, the focus is on the Human (but
not in an anthropocentric way) and its relation to his environment. She
defines her work by drawing contrasts, opposing her own approach to
other concepts, and especially those of the French intellectual Luc
Ferry. Posthumus uses him as a starting point to define French
“écocritique” and its intellectual context; a concept she will later
reject. As a well known French thinker, L. Ferry influenced the very
beginning of the discipline in France, and this (negative) influence
partly explains the difficulties faced by the ecocritique in the attempt
to find its place in the intellectual sphere. In this way, many keys to
the understanding of *French Ecocritique* are to be found in its
relation to L. Ferry’s *Le nouvel ordre écologique*&nbsp;: S. Pothumus calls
for a “culturally situated” [-@posthumus_french_2017, p.5] ecocritic approach, and looks for ways out
of the universal humanism. She thus regularly reasserts the fact that we
can’t separate matters of language from matters of cultures, their are
interdependent and she need to take this into account if she aims to
create global scale’s arguments. Justifying her work by this dichotomous
structure, she quickly puts aside Ferry’s secular and liberal humanism,
as well as a categorizing thought which separate nature from culture.

S. Posthumus will then seek to create arguments and concepts without
falling into the trap of the universal and anthropocentric humanism, and
she manages to do so by using dynamic and flexible concepts,
transformations, which are intrinsically organic and always in progress.
All four chapters include a literary theorist and a fiction writer in
order to illustrate the four concepts.

How can we create an ecological subjectivity and why is it essential to
include the environment in a definition of ourselves&nbsp;? That’s the
question answered in the first chapter. Posthumus uses the concept of
“écosophie”, created by Arne Naess and developed by Felix Guattari and
the fiction of Marie Darrieussecq.

Guattari’s ambition is to construct an ethic and aesthetic paradigm,
through three ecologies&nbsp;: the mental (linked to human psyche), the
social (which consists of all societal practices) and the environmental
(our relation to nature). What distinguish his work, and what connects
him to Posthumus, is him seeing subjectivity as a steadily developing
process. Like Posthumus, he refuses the idea of a fixed identity, of a
transcendent self. That is one of the key points we can find in *French
Ecocritique* if we focus on the author thoughts rather than those of the
authors she analyses&nbsp;: if we want to rethink the human and its relation
to its surroundings, it is essential not to deny but to acknowledge the
materiality and the immanence of human experience (while is also an
animal experience). It echoes the idea of ending the human
exceptionalism; an idea repeatedly presented in the book. It also
opposes Ferry’s nature-culture distinction. In contrast to him, Guattari
and Darrieussecq erase this separation, by presenting an immanent and
unstable subjectivity&nbsp;: the experience is necessarily embodied. And it
is precisely this instability of fiction (the author uses autofiction,
which interfere with literary conventions), which is nevertheless
focused on individual experiences, that gives birth to the concept of
ecological subjectivity. This union of theory and fiction seems
essential to make a bridge between the local and the global, the
abstract and the tangible.

This texts allow Posthumus to elaborate new theories, but also to
illustrate her philosophy. Guattari’s ecosophie echoes the book’s
concepts&nbsp;: just as the spheres of the ecocritique thought, the author's
concepts are codependent and make sense in this very relation of
dependence.

This first chapter presents the structure Posthumus is going to follow&nbsp;:
she starts with a brief presentation of the authors (the “portraits”),
then she focuses on theory, then on fiction, and she ends by uniting
both. This structure helps her to clarify her thought and arguments.

Following the same plan in chapter two, the authors asks herself what it
means to live ecologically [-@posthumus_french_2017, p.10], which leads her to create the concept of
“ecological dwelling”. Loyal to her ambitions and beliefs, she insists
again that her book and the other works she studies are not restricted
to concrete ideas of actions, aiming to fight environmental problems. I
too insist on this point (and the book will force me to come back to it)
because it might be the very reason why Posthumus work is so special in
this discipline, while permitting her to reassert the literary
importance of her argument. Following this idea, this chapter will not
be about self sufficient houses. Just as the ecological subjectivity,
the ecological dwelling refers the materiality of human experience,
which is first experienced through the body and how it receives,
perceives and interacts with its environment&nbsp;; it being natural or
cultural, because it now means nothing to separate both. Concerning
theory, S. Posthumus leans on Michel Serres and his idea of “natural
contract”. He uses the peasant figure (a strong image in a french
context) to illustrate, at a local scale, some sort of symbiosis and
co-dependence; an image on which he relies to then erect an ecological
dwelling at a global scale. The individual scale is also very important
in the fiction part. Indeed, Marie-Hélène Lafon portrays, through
individual life stories, the difficulty of a rural life and the threat
hanging over the identity of the “peasant”. Her writing is realistic,
nostalgia free, while showing how life is “composed, renewed and
regenerated” [-@posthumus_french_2017, p.10], which joins the idea of transformation supported by S.
Posthumus. That is also the bond between the theorist and the writer&nbsp;:
they both intend to rethink our relation to the earth throughout the
figure of the peasant. Moreover, the embodiment of the experience is at
the heart of both discourses, both works. In M.-H. Lafon’s literature, the
reader is deeply involved with sensations and perceptions&nbsp;; Serres
depicts the earth as a body constantly interacting with its environment
(which includes humans). Finally, Serres and Lafon both point out to the
embodiment of language&nbsp;: they experiment it as a storytelling place, as
an ecological dwelling&nbsp;: "the French language represents an oikos for
both Lafon and Serres, and so becomes one of ways in which they live and
convey a sense of embodiment" [-@posthumus_french_2017, p.164]. It also echoes Posthumus use of
language, because by choosing to write in English while keeping some
concepts and quotes in French, she highlights the fact that the language
is tied to culture and to material processes. In all cases, taking the
environment into account involves the language&nbsp;: "the reader becomes
aware of the richness of the french language, its ability to go beyond
abstract and universal concepts and describe the thick vicosity of the
real world without necessarily imposing order on it" [-@posthumus_french_2017, p.92].

The priority of the material world continues and is reinforced in the
third chapter, which asks what political system could allow us to
include the non-human. Still following her leitmotiv, S. Posthumus
maintains that the ecological politics need to be critical of the
environmentalism while proposing a new ecological paradigm. A paradigm
which would not get trapped in the fight between humanism and
anti-humanism (trap that Ferry does not avoid). The author follows Bruno
Latour’s philosophy, for whom we have to make room in the cultural
systems for the non-human, but without attributing an intrinsic value to
nature, unlike how it is done in the english traditional thought. He
also rejects the nature culture separation , and the distinction between
"matters of facts from matters of concern" [-@posthumus_french_2017, p.102], and points out the french
particularity while separating himself from L. Ferry. All theses beliefs
are to be found in Jean-Christophe Rufin’s books, whose work is analysed
in this chapter. His stories are also critical of the english
environmentalism while asking the question of the place taken by the non
human in the contemporary democratic systems.

The concept of ecological politics emerges as a powerful way to respond
to global issues by using french singularity concerning our relation to
nature and the environment. It’s also the first part of the book which
critics capitalism without reserve. It is meaningful that the author
seems to have waited to tackle head on the political sphere and to thus
attack our current economic system. Of course, her disapproval is
implicit from the beginning, but saying it outloud has a special meaning
in this chapter. Indeed, the capitalist system is shaped to encourage
greenwashing. Yet, it does not actually change the system, and this
change, which is to find a way out of capitalism, is central the
Posthumus’ aspiration of a “common world”. That is why she fits more in
Latour’s philosophy, instead of Rufin’s. The second, although also
critical of capitalism, wishes to come back to an universal capitalism&nbsp;;
Latour’s argues for an ecological politic which would integrate the non
human to the democratic system, while getting out of capitalism&nbsp;: “by
giving voice to a common world, they reveal a path toward deeper
systemic change than simply «&nbsp;greening&nbsp;» current «&nbsp;economic&nbsp;»
pratices” [-@posthumus_french_2017, p.124].

This concept of ecological politics allows us to conceive groundbreaking
changes which are today essentials&nbsp;: by integrating the non-human to
political matters (Latour calls for a parliament of things) we would
give voice to a larger numbers of living things, we would extent the
power and responsibility of the polis. First, the non-living cannot
continue to be set apart, that needs to be said, and it would naturally
helps us to get out of a anthropocentric humanism. Finally, it would
help us to imagine a way out of capitalism. And it’s important to say a
few world about my use of the verb “imagine”, which refocuses on
literature. Indeed, if we want to conceive new worlds, we need to use
storytelling, a narrative voice, to consider history as a fiction&nbsp;:
"the novels create room for ecological readings that identify other
ways of viewing and experiencing the world, that bring an awareness of
political issues to the text rather than reducing the text to a single
political message" [-@posthumus_french_2017, p.125]. Literature and fictions are vitals to the
conception of changes as enormous as these ones, because it allows each
and everyone of us to feel and experiences what is still unfamiliar.

The importance of the writing and reading experiences is at the heart of
the last chapter (and concept), entitled “ecological ends”. In these
last lines, the “human” is relevant mainly because he is a “homo
literatus”. Michel Houellebecq, well known for his overall pessimist
point of view, describes different posthuman conditions in his fictions,
or at least post humanity as we know it today. This chapter also focus
on the role this ends might play, in terms of their possible impact on
the popular imagination. Thinking about the very likely extinction of
the human species is a highly unpleasant thought experiment , but with a
great potential&nbsp;: it allows us to replace the human to its original
condition, which is a biological entity, a part of the living world.
This conception leaves no room for the human exceptionalism and it is
this very ecological end of humans as a privileged specie that
Houellebecq and Schaeffer represent (but without using apocalyptic
views, the books examine the human life possible transformations,
including cloning). Nevertheless, this material view of humans puts them
at risk of falling into biological reductionism, and that is what
distinguishes the authors. If Schaeffer manages to "articulates a non
dualistic view of the nature culture continuum" (a concept that, as we
know, Pothumus cherishes), Houellebecq chooses to "illustrate a
problematic determinism according to which humans and animals are bound
to the « laws of nat&nbsp;»" [-@posthumus_french_2017, p.128]. These questions are part of the broader subject
of posthumanism, which is itself divided in two parts&nbsp;: one approaches
the human / machine binarism&nbsp;; the other terminates the human / animal
binarism, and its goal to rethink our ethical responsibility concerning
animals. Furthermore, the notion of animality is very relevant in a
french context. Schaeffer conceptualizes it through the question of
ethics, and advocates for a more systematic consideration of scientific
and social datas, for taking into account the contexts of the studies,
in order to reach a non-binary and non-dualistic view of humans and
animals. “The living” also emerges as a concept, precisely to put a word
on this absence of distinction between humans and animals, and between
them and their environment. In Schaeffer’s mind, evolution cannot be
seen as theological and based on the principle of natural selection.
This position can be taken as another way of asking the question of our
survival.

Despite this philosophy, Houellebecq sees natural selection as
impossible to avoid and reduces human to its worst capacities. But its
fiction remain relevant in the analysis of literature. In her studies
Posthumus distinguishes two different and interesting conceptions, which
are interesting because they are differents&nbsp;: on the one hand
Houellebecq offers a humanistic belief in literature, which appears as
essential to the human condition and experience&nbsp;; and on the other hand
it does stop him from depicting a posthumanist word in which literature
does not exist anymore&nbsp;: "they reveal a productive tension between a
posthumanist view of humans as part of the natural world and a humanist
rethinking of the role of literature and stories in the larger context
of humans as species" [-@posthumus_french_2017, p.156]. In this work, literature is the supreme human
capacity, but a capacity that is neither biological nor everlasting.
Houellebecq’s vision allows us to enter a future in which their is no
new stories, no new narratives, while Schaeffer proposes to extend these
notions. He is in favor of a broader vision of these terms, which would
includes movies, video games, digital writing and all other contemporary
fictional productions, which are not handwritten (without shutting this
one out).

This last vision concludes the presentation of the ecological ends and
fits with the idea of a more comparatist approach, and permits to
increase the ways of feeling and experiencing the word.

S. Posthumus finishes her book by summarizing it, and by going through
the concepts we followed&nbsp;: "While critical of the humanistic self, my
ecocritical approach includes the human without resurrecting the
universal (white, male) human subject. It does so by emphasizing
processes of subjectivity, examining the modes of embodiment, opening
politics up to the agency of non-humans, and accepting science as a way
of making forms of life matter, as a way of «&nbsp;rendering&nbsp;» certain
material processes and not other" [-@posthumus_french_2017, p.159]. Her work has the strength of opening
new paths to literary approaches, theoretical as well as fictional,
while drawing attention on the great role these disciplines can and must
play in the contemporary context of ecological and social crisis (and
especially from a comparatist perspective). That said, the study is
essentially based on white male books, and focuses on French identity,
which is already traditionally enlightened and highly considered. But
Posthumus points these facts out herself, and we need to keep in mind
that the french characteristics are a starting point to reach the global
scale.

Literature then arises as vital in the way we are thinking about the
human and could even be a great help in the near future if we want to
survive as a specie, as a medium to create and spread experiences.
Reuniting fiction and theory as she does appears extremely profitable,
yet we can be critical about her drastic way of rejecting any form of
environmentalism and the concrete ideas the humanity could benefit in
its attempt to make a radical change. Rethinking subjectivities,
replacing the human as a biological entity, creating new posthumanist
visions which would integrate the non living world, all of this needs to
be embodied in concrete experiences.

#Bibliography
